package com.hng.demo.controller;

import javax.validation.constraints.NotNull;

import com.hng.demo.model.Product;
import com.hng.demo.service.ProductService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/product")
public class ProductController {
    private ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService; 
    }

    @GetMapping(value = {"","/"})
    public @NotNull Iterable<Product> getProducts() {
        return productService.getAllProducts();
    }
}
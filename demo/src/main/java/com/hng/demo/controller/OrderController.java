package com.hng.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.hng.demo.dto.OrderProductDto;
import com.hng.demo.model.Order;
import com.hng.demo.model.OrderProduct;
import com.hng.demo.service.OrderProductService;
import com.hng.demo.service.OrderService;
import com.hng.demo.service.ProductService;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
@RestController
@RequestMapping("api/v1/order")
public class OrderController {
    ProductService productService;
    OrderService orderService;
    OrderProductService orderProductService;

    public OrderController(ProductService productService, OrderService orderService, OrderProductService orderProductService) {
        this.productService = productService;
        this.orderService = orderService;
        this.orderProductService = orderProductService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public @NotNull Iterable<Order> list() {
        return this.orderService.getAllOrders();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @NotNull Optional<Order> getOrderById(@PathVariable Long id) {
        return this.orderService.getOrder(id);
    }

    @PostMapping
    public ResponseEntity<Long> doThis(@RequestBody OrderForm form) {
        List<OrderProductDto> fromDtos = form.getProductOrder();
        Order order;
        if(!form.getOrderId().isPresent()) {
            order = new Order();
            order = this.orderService.create(order);
        } else {
            order = orderService.getOrder(form.getOrderId().get()).get();
        }
        List<OrderProduct> orderProducts = new ArrayList<>();

        for(OrderProductDto dto: fromDtos ) {
            orderProducts.add(orderProductService.save(new OrderProduct(order, dto.getProduct(), dto.getQuantity())));
        }

        String uri = ServletUriComponentsBuilder.fromCurrentServletMapping().path("/orders/{id}")
        .buildAndExpand(order.getId())
        .toString();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Location", uri);

        order.setOrderProducts(orderProducts);
        this.orderService.update(order);
    
        return new ResponseEntity<>(order.getId(), headers, HttpStatus.CREATED);
    }

    public static class OrderForm {
        private Optional<Long> orderId;
        private List<OrderProductDto> productOrder;

        /**
         * @return the orderId
         */
        public Optional<Long> getOrderId() {
            return orderId;
        }

        /**
         * @param orderId the orderId to set
         */
        public void setOrderId(Optional<Long> orderId) {
            this.orderId = orderId;
        }


        /**
         * @return the productOrder
         */
        public List<OrderProductDto> getProductOrder() {
            return productOrder;
        }

        /**
         * @param productOrder the productOrder to set
         */
        public void setProductOrder(List<OrderProductDto> productOrder) {
            this.productOrder = productOrder;
        }
    }
}
package com.hng.demo.repository;

import com.hng.demo.model.OrderProduct;

import org.springframework.data.repository.CrudRepository;

public interface OrderProductRepository extends CrudRepository<OrderProduct, Long>{}
package com.hng.demo.dto;

import com.hng.demo.model.Product;


public class OrderProductDto {
    private Product product;
    private int quantity;

    // OrderProductDto() {

    // }

    // OrderProductDto(Product product, int quantity) {
    //     this.product = product;
    //         this.quantity = quantity;
    // }
    /**
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
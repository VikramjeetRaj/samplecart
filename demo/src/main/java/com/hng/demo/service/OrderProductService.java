package com.hng.demo.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.hng.demo.model.OrderProduct;

import org.springframework.validation.annotation.Validated;

@Validated
public interface OrderProductService {
    OrderProduct save(@NotNull(message = "The products for order cannot be null.") @Valid OrderProduct orderProduct);
}
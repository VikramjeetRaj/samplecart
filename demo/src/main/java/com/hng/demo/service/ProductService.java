package com.hng.demo.service;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.hng.demo.model.Product;

import org.springframework.validation.annotation.Validated;

@Validated
public interface ProductService {
    @NotNull Iterable<Product> getAllProducts();
    Product getProduct(@Min(value = 1L, message = "Invalid Product Id.") long id);
    Product save(Product product);
}

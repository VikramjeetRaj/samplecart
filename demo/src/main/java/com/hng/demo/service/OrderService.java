package com.hng.demo.service;
import org.springframework.validation.annotation.Validated;

import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.hng.demo.model.Order;

@Validated
public interface OrderService {

    @NotNull Iterable<Order> getAllOrders();

    Order create(@NotNull(message = "The order cannot be null.") @Valid Order order);

    void update(@NotNull(message = "The order cannot be null.") @Valid Order order);

    Optional<Order> getOrder(@NotNull(message = "The order cannot be null.") @Valid Long id);
}
package com.hng.demo.service;

import com.hng.demo.model.OrderProduct;
import com.hng.demo.repository.OrderProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional

public class OrderProductServiceImpl implements OrderProductService {

    private OrderProductRepository orderProductRepository;

    public OrderProductServiceImpl(OrderProductRepository orderProductRepository) {
        this.orderProductRepository = orderProductRepository;
    }
    @Override
    public OrderProduct save(OrderProduct orderProduct) {
        return this.orderProductRepository.save(orderProduct);
    }
    
}